

data_hosps_cum <- c(2,3,3,
                    4,6,7,7,8, 10,10,11,11,12, 15,17,19,19,19, 27,31,38,40,45, 51,56,65,78,86, 90,96,105,111,117, 124,
                    125,128,132,134,135, 136,136,136,137,137, 138,139,142,144,144, 144,144,146,147,148, 148,148,148,148,148, 149)
data_hosps <- c(2,1,0,
                1,2,1,0,1,    2,0,1,0,1,      3,2,2,0,0,      8,4,7,2,5,       6,5,9,13,8,    4,6,9,6,6,        7,
                1,3,4,2,1,   1,0,0,1,0,       1,1,3,2,0,      0,0,2,1,1,      0,0,0,0,0,      1)

data_deaths <- c(0,0,0,
                 0,0,0,0,0,   0,0,0,0,0,     0,0,0,0,0,      1,1,0,0,1,   2,1,0,0,1,      2,0,4,2,5,        1,
                 3,2,0,3,4,   0,4,3,4,5,     3,2,3,2,2,      1,1,1,0,3,   2,1,1,1,0,      0)

data_cases <- c(0,0,0,
                0,4,3,8,5,   9,0,3,3,7,     16,24,10,18,20,  65,54,57,48,61,  42,37,64,52,57,  53,78,23,22,71,  71,
                54,41,60,28,13,  69,50,58,17,16,  18,11,17,36,30,  19,16,6,10,28,  18,11,20,10,4,  9)







casedeath_dates <- seq(from=as.Date("2020/02/27"), as.Date("2020/04/26"), by=1)



n_simuls <- 1000
max_days <- 350

epsilon <- 1/3.7 # E to Ip
mu_p <- 1/1.5 # Ip to Ia/Ips/Ims/Iss
mu <- 1/2.3 # Ia/Ips/Ims to recovery; Iss to hospitalization (assumed to be equal)
p_aC <- 0.5 # Probability to be asymptomatic: children
p_aA <- 0.5 # Probability to be asymptomatic: adults
p_aS <- 0.5 # Probability to be asymptomatic: seniors
p_msC <- 0 # If symptomatic, probability to have mild symptoms (but excluding paucisymptomatic): children
p_msA <-0.7 # ...
p_msS <- 0.6 # ...
p_ssC <- 0 # If symptomatic, probability to have severe symptoms: children
p_ssA <- 0.03 #0.1 #...
p_ssS <- 0.35 #0.2 # ...
p_ICUC <- 0 # If hospitalized, probability to go to ICU: children
p_ICUA <- 0.25 #0.36 # ...
p_ICUS <- 0.2 #0.2 # ...
lambdaHRC <- 0 # H to R: children
lambdaHRA <- 0.072 # ...
lambdaHRS <- 0.022 # ...
lambdaHDC <- 0
#lambdaHDA <- 2*0.0042 # ...
#lambdaHDS <- 4*0.014 # ...
lambdaICURC <- 0
lambdaICURA <- 0.05 # ...
lambdaICURS <- 0.036 # ...
lambdaICUDC <- 0
#lambdaICUDA <- 2*0.0074 # ...
#lambdaICUDS <- 4*0.029 # ...
rb <- 0.51 # relative infectiousness of prodomic, asymptomatic and paucisymptomatic individuals


#beta0 <- 0.8 # 0.74

relCC <- 2
relCA <- 0.5
relCS <- 0.2
relAS <- 0.4
relSS <- 1

#schoolCC <- 0.05
#schoolCA <- 0.8
#workAA <- 0.5
#shopAA <- 0.5 #0.5
#social1 <- 0.75
#social2 <- 0.75*0.65
restaurantAA <- 0.8




lambdaHDA <- 0.009483796
lambdaHDS <- 0.02826953
lambdaICUDA <- 0.016140518
lambdaICUDS <- 0.27265618
start_date_fixed <- as.Date("2020/02/19") #as.Date("2020/02/08")
beta0 <- 0.7238953 #0.8945111
schoolCC <- 0.04333793 #U0.05488696
schoolCA <- 0.5064165 #0.9311092
workAA <- 0.4321602 #0.2116836
shopAA <- 0.3655506 #0.3898732
social1 <- 0.8725603 #0.8950454
social2 <- 0.6879610 #0.5192640
inseed <- 18 #4

#tracA <- 0.1
#tracing <- c(1,1,1,tracA^2,tracA,1)
#tracing <- c(0.71,0.42,0.71,0.42,0.42,0.71)
#tracS <- 1 #0.1
#tracing <- c(1,1,tracS,1,tracS,tracS^2)
#tracing <- 0.42*c(1,1,1,1,1,1)
tracing <- c(1,0.71,0.05,0.42, 0.05, 0.71)

daily_hosps <- matrix(nrow=n_simuls, ncol=max_days)

daily_deaths <- matrix(nrow=n_simuls, ncol=max_days)

daily_infections <- matrix(nrow=n_simuls, ncol=max_days)

cumul_infections <- matrix(nrow=n_simuls, ncol=max_days)

daily_inhospital <- matrix(nrow=n_simuls, ncol=max_days)

daily_inICU <- matrix(nrow=n_simuls, ncol=max_days)

Repr <- matrix(nrow=n_simuls, ncol=max_days)

canton <- "BE"
modelsolution <- "combined"


for (i in 1:n_simuls) {
  source("model_GE.R")
  daily_hosps[i,] <- newhosps_C+newhosps_A+newhosps_S
  daily_deaths[i,] <- newdeaths_C+newdeaths_A+newdeaths_S
  daily_infections[i,] <- newcases_C+newcases_A+newcases_S
  cumul_infections[i,] <- cumsum(daily_infections[i,])
  daily_inhospital[i,] <- HC+HA+HS+ICUC+ICUA+ICUS
  daily_inICU[i,] <- ICUC+ICUA+ICUS
  Repr[i,] <- Re
}

plot(x=seq(from=as.Date("2020/02/18"), to=as.Date("2020/02/18")+max_days-1, by=1), y=rep(2000, length=max_days),  ylim=c(0,max(max(daily_hosps),data_hosps)))
#plot(x=seq(from=as.Date("2020/02/18"), to=as.Date("2020/05/01"), by=1), y=rep(2000, length=74),  ylim=c(0,max(max(daily_hosps[,(1:75)]),data_hosps)))
for (i in (1:n_simuls)) {
  lines(x=seq(from=as.Date("2020/02/25"), to=as.Date("2020/02/25")+max_days-1, by=1), y=daily_hosps[i,], col="green", lwd=0.5)
}
lines(x=seq(from=as.Date("2020/02/25"), to=as.Date("2020/02/25")+max_days-1, by=1), y=apply(daily_hosps,2,sum)/n_simuls, col="darkgreen", lwd=2)
lines(x=casedeath_dates, y=data_hosps, col="gray", lwd=2, lty="dashed")



plot(x=seq(from=as.Date("2020/02/18"), to=as.Date("2020/02/18")+max_days-1, by=1), y=rep(2000, length=max_days), xlim=c(as.Date("2020/02/01"),as.Date("2020/12/31")), ylim=c(0,max(max(daily_deaths),data_deaths)))
#plot(x=seq(from=as.Date("2020/02/18"), to=as.Date("2020/05/01"), by=1), y=rep(2000, length=74), ylim=c(0,max(max(daily_deaths[,1:75]),data_deaths)))

for (i in (1:n_simuls)) {
  lines(x=seq(from=as.Date("2020/02/18"), to=as.Date("2020/02/18")+max_days-1, by=1), y=daily_deaths[i,], col="gray", lwd=0.5)
}
lines(x=seq(from=as.Date("2020/02/18"), to=as.Date("2020/02/18")+max_days-1, by=1), y=apply(daily_deaths,2,sum)/n_simuls, col="black", lwd=2)
lines(x=casedeath_dates, y=data_deaths, col="gray", lwd=2, lty="dotted")


plot(x=seq(from=as.Date("2020/02/18"), to=as.Date("2020/02/18")+max_days-1, by=1), y=rep(50000, length=max_days), xlim=c(as.Date("2020/02/01"),as.Date("2020/12/31")), ylim=c(0,max(max(daily_infections),data_cases)))
#plot(x=seq(from=as.Date("2020/02/18"), to=as.Date("2020/02/18")+max_days-1, by=1), y=rep(50000, length=max_days), xlim=c(as.Date("2020/02/18"),as.Date("2020/05/01")), ylim=c(0,max(max(daily_infections[,(1:75)]),data_cases)))

for (i in (1:n_simuls)) {
  lines(x=seq(from=as.Date("2020/02/18"), to=as.Date("2020/02/18")+max_days-1, by=1), y=daily_infections[i,], col="violet", lwd=0.5)
}
lines(x=seq(from=as.Date("2020/02/18"), to=as.Date("2020/02/18")+max_days-1, by=1), y=apply(daily_infections,2,sum)/n_simuls, col="darkviolet", lwd=2)
lines(x=casedeath_dates, y=data_cases, col="gray", lwd=2, lty="dotted")







plot(x=seq(from=as.Date("2020/02/18"), to=as.Date("2020/02/18")+max_days-1, by=1), y=rep(1000000, length=max_days), xlim=c(as.Date("2020/02/01"),as.Date("2020/12/31")), ylim=c(0,1036000))

for (i in (1:n_simuls)) {
  lines(x=seq(from=as.Date("2020/02/18"), to=as.Date("2020/02/18")+max_days-1, by=1), y=cumul_infections[i,], col="orange", lwd=0.5)
}
lines(x=seq(from=as.Date("2020/02/18"), to=as.Date("2020/02/18")+max_days-1, by=1), y=apply(cumul_infections,2,sum)/n_simuls, col="red", lwd=2)
lines(x=casedeath_dates, y=cumsum(data_cases), col="gray", lwd=2, lty="dotted")
#lines(x=c(as.Date("2020/04/06"), as.Date("2020/04/10")), y=c(0.035, 0.035)*inpop, col="black", lwd=3)
#lines(x=c(as.Date("2020/04/06"), as.Date("2020/04/10")), y=c(0.016, 0.016)*inpop, col="gray", lwd=1.5)
#lines(x=c(as.Date("2020/04/06"), as.Date("2020/04/10")), y=c(0.054, 0.054)*inpop, col="gray", lwd=1.5)
#lines(x=c(as.Date("2020/04/14"), as.Date("2020/04/17")), y=c(0.055, 0.055)*inpop, col="black", lwd=3)
#lines(x=c(as.Date("2020/04/14"), as.Date("2020/04/17")), y=c(0.033, 0.033)*inpop, col="gray", lwd=1.5)
#lines(x=c(as.Date("2020/04/14"), as.Date("2020/04/17")), y=c(0.077, 0.077)*inpop, col="gray", lwd=1.5)


plot(x=seq(from=as.Date("2020/02/18"), to=as.Date("2020/02/18")+max_days-1, by=1), y=rep(510000, length=max_days), xlim=c(as.Date("2020/02/01"),as.Date("2020/12/31")), ylim=c(0,max(max(daily_inhospital),data_inhosp)))

for (i in (1:n_simuls)) {
  lines(x=seq(from=as.Date("2020/02/18"), to=as.Date("2020/02/18")+max_days-1, by=1), y=daily_inhospital[i,], col="green", lwd=0.5)
}
lines(x=seq(from=as.Date("2020/02/18"), to=as.Date("2020/02/18")+max_days-1, by=1), y=apply(daily_inhospital,2,sum)/n_simuls, col="darkgreen", lwd=2)
#lines(x=casedeath_dates, y=data_inhosp, col="gray", lwd=2, lty="dotted")


plot(x=seq(from=as.Date("2020/02/18"), to=as.Date("2020/02/18")+max_days-1, by=1), y=rep(510000, length=max_days), xlim=c(as.Date("2020/02/01"),as.Date("2020/12/31")), ylim=c(0,max(max(daily_inICU),data_inICU)))

for (i in (1:n_simuls)) {
  lines(x=seq(from=as.Date("2020/02/18"), to=as.Date("2020/02/18")+max_days-1, by=1), y=daily_inICU[i,], col="pink", lwd=0.5)
}
lines(x=seq(from=as.Date("2020/02/18"), to=as.Date("2020/02/18")+max_days-1, by=1), y=apply(daily_inICU,2,sum)/n_simuls, col="darkred", lwd=2)
#lines(x=casedeath_dates, y=data_inICU, col="black", lwd=2, lty="dotted")
lines(x=seq(from=as.Date("2020/02/18"), to=as.Date("2020/02/18")+max_days-1, by=1), y=rep(118, length=max_days), col="red", lty="dashed", lwd=2)


plot(x=seq(from=as.Date("2020/02/18"), to=as.Date("2020/02/18")+max_days-1, by=1), y=rep(510000, length=max_days), xlim=c(as.Date("2020/02/18"),as.Date("2020/12/31")), ylim=c(0,5))


for (i in (1:n_simuls)) {
  lines(x=seq(from=as.Date("2020/02/18"), to=as.Date("2020/02/18")+max_days-1, by=1), y=Repr[i,], col="cyan", lwd=0.5)
  
}
lines(x=seq(from=as.Date("2020/02/18"), to=as.Date("2020/02/18")+max_days-1, by=1), y=apply(Repr,2,sum)/n_simuls, col="darkblue", lwd=2)
lines(x=seq(from=as.Date("2020/02/18"), to=as.Date("2020/02/18")+max_days-1, by=1), y=rep(1, length=max_days), col="black", lty="dashed", lwd=1)





